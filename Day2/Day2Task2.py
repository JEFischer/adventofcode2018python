import argparse
import pytest
import collections
from typing import Set


def compute(s: str) -> str:
    for line in s.splitlines():
        for otherLine in s.splitlines():
            if line != otherLine:
                SubstringSetLine = toSubstringSet(line)
                SubstringSetOtherLine = toSubstringSet(otherLine)
                if SubstringSetLine.intersection(SubstringSetOtherLine):
                    return SubstringSetLine.intersection(SubstringSetOtherLine).pop()

def toSubstringSet(s: str) -> Set[str]:
    SubstringSet = set()
    for i in range(len(s)):
        SubstringSet.add(s[:i] + s[i + 1:])
    return SubstringSet

@pytest.mark.parametrize(
    ('input_s', 'expected'),
    (
        ('', set()),
        ('a', {''}),
        ('bar', {'ar', 'br', 'ba'}),
        ('foo', {'oo', 'fo'}),
    ),
)
def testToSubstringSet(input_s: str, expected: Set[str]) -> None:
    assert toSubstringSet(input_s) == expected

@pytest.mark.parametrize(
    ('input_s', 'expected'),
    (
        ('abcde\nfghij\nklmno\npqrst\nfguij\naxcye\nwvxyz', 'fgij'),
    ),
)
def test(input_s: str, expected: int) -> None:
    assert compute(input_s) == expected


def main() -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('data_file')
    args = parser.parse_args()

    with open(args.data_file) as f:
        print(compute(f.read()))

    return 0


if __name__ == '__main__':
    exit(main())
