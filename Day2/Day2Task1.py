import argparse
import pytest
import collections
from typing import Counter


def countLetters(s: str) -> dict:
    l2, l3 = 0, 0
    char_counts = collections.Counter(s)
    if 2 in char_counts.values():
        l2 = 1
    if 3 in char_counts.values():
        l3 = 1
    return {'e2': l2, 'e3': l3}


def compute(s: str) -> int:
    c2, c3 = 0, 0
    for line in s.splitlines():
        result = countLetters(line)
        c2 += result['e2']
        c3 += result['e3']
    return c2 * c3


@pytest.mark.parametrize(
    ('input_s', 'expected2', 'expected3'),
    (
        ('abcdef', 0, 0),
        ('bababc', 1, 1),
        ('abbcde', 1, 0),
        ('abcccd', 0, 1),
        ('aabcdd', 1, 0),
        ('abcdee', 1, 0),
        ('ababab', 0, 1),
    ),
)
def testCountLetters(input_s: str, expected2, expected3: int) -> None:
    sums = countLetters(input_s)
    assert (sums['e2'] == expected2) & (sums['e3'] == expected3)


@pytest.mark.parametrize(
    ('input_s', 'expected'),
    (
        ('abcdef\n'
         'bababc\n'
         'abbcde\n'
         'abcccd\n'
         'aabcdd\n'
         'abcdee\n'
         'ababab', 12),
    ),
)
def test(input_s: str, expected: int) -> None:
    assert compute(input_s) == expected


def main() -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('data_file')
    args = parser.parse_args()

    with open(args.data_file) as f:
        print(compute(f.read()))

    return 0


if __name__ == '__main__':
    exit(main())
