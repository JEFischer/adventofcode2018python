import argparse
import pytest
import collections
from typing import Set


def compute(s: str) -> str:
    fabric:  DefaultDict[int, DefaultDict[int, int]]
    fabric = collections.defaultdict(lambda: collections.defaultdict(int))
    for line in s.splitlines():
        arguments = line.split()
        xPos, yPos = arguments[2].split(',')
        width, height = arguments[3].split('x')
        xPos, yPos = int(xPos), int(yPos[:-1])
        width, height = int(width), int(height)
        for i in range(xPos, xPos + width):
            for j in range(yPos, yPos + height):
                fabric[i][j] += 1

    overlaps = 0
    for _, row in fabric.items():
        for _, count in row.items():
            if count >= 2:
                overlaps += 1

    return overlaps


@pytest.mark.parametrize(
    ('input_s', 'expected'),
    (
        (
            '#1 @ 1,3: 4x4\n'
            '#2 @ 3,1: 4x4\n'
            '#3 @ 5,5: 2x2\n',

            4,
        ),
    ),
)
def test(input_s: str, expected: int) -> None:
    assert compute(input_s) == expected


def main() -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('data_file')
    args = parser.parse_args()

    with open(args.data_file) as f:
        print(compute(f.read()))

    return 0


if __name__ == '__main__':
    exit(main())
