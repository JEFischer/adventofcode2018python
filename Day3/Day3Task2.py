import argparse
import pytest
from typing import NamedTuple


class Claim(NamedTuple):
    id: str
    xPos: int
    yPos: int
    width: int
    height: int

    @classmethod
    def parse(cls, s: str) -> 'Claim':
        arguments = s.split()
        id = arguments[0]
        xPos, yPos = arguments[2].split(',')
        width, height = arguments[3].split('x')
        xPos, yPos = int(xPos), int(yPos[:-1])
        width, height = int(width), int(height)
        return cls(id, xPos, yPos, width, height)

    def overlaps(self, other: 'Claim') -> bool:
        return (
            self.xPos < other.xPos + other.width
            and self.xPos + self.width > other.xPos
            and self.yPos < other.yPos + other.height
            and self.yPos + self.height > other.yPos
        )


def compute(s: str) -> str:
    claims = [Claim.parse(line)for line in s.splitlines()]
    for claim in claims:
        notOverlapsOtherClaim = True
        for otherClaim in claims:
            if claim != otherClaim:
                if claim.overlaps(otherClaim):
                    notOverlapsOtherClaim = False
                    break
        if notOverlapsOtherClaim:
            return claim.id


@pytest.mark.parametrize(
    ('input_s', 'expected'),
    (
        (
            '#1 @ 1,3: 4x4\n'
            '#2 @ 3,1: 4x4\n'
            '#3 @ 5,5: 2x2\n',

            '#3',
        ),
    ),
)
def test(input_s: str, expected: int) -> None:
    assert compute(input_s) == expected


def main() -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('data_file')
    args = parser.parse_args()

    with open(args.data_file) as f:
        print(compute(f.read()))

    return 0


if __name__ == '__main__':
    exit(main())
